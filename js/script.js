"use strict";

function createNewUser() {
  const newUser = {
    firstName: prompt(`Enter you name`),
    lastName: prompt(`Enter you last name`),
    birthday: prompt(`Enter you date of birth (format: dd.mm.yyyy)`),
    getLogin() {
      return (this.firstName[0] + this.lastName).toLowerCase();
    },
    getAge() {
      return Math.floor(
        (new Date() -
          new Date(
            this.birthday.slice(3, 5) +
              "," +
              this.birthday.slice(0, 2) +
              "," +
              this.birthday.slice(6, 10)
          )) /
          (1000 * 60 * 60 * 24 * 365)
      );
    },
    getPassword() {
      return (
        this.firstName[0].toUpperCase() +
        this.lastName.toLowerCase() +
        this.birthday.slice(6, 10)
      );
    },
  };
  return newUser;
}

const newUser = createNewUser();
console.log(newUser);
console.log(newUser.getAge());
console.log(newUser.getPassword());
